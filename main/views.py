from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Matkul

def profil(request):
    return render(request, 'main/profil.html')

def proyek(request):
    return render(request, 'main/proyek.html')

def tentang(request):
    return render(request, 'main/tentang.html')

def story1(request):
    return render(request, 'main/hendraHeroku.html')

def formMatkul(request):
    response = {'input_form' : Input_Form}
    return render(request, 'main/formMatkul.html', response)

def tambah_matkul(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/readmatkul')
    else:
        return HttpResponseRedirect('/formMatkul')

def readmatkul(request):
    matkul = Matkul.objects.all()
    response = {'matkul' : matkul}

    return render(request,'main/profil.html', response)
