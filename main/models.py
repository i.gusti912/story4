from django.db import models

# Create your models here.
class Matkul(models.Model):
    name = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=3)
    tahun = models.CharField(max_length=4)
    deskripsi = models.CharField(max_length=2000)