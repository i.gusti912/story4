from django.urls import include, path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.profil, name='profil'),
    path('proyek/', views.proyek, name='proyek'),
    path('tentang-saya/', views.tentang, name='tentang'),
    path('story1/', views.story1, name='story1'),
    path('formMatkul/', views.formMatkul, name='formMatkul'),
    path('formMatkul/tambah_matkul', views.tambah_matkul),
    path('readmatkul/', views.readmatkul, name='readmatkul'),
]
