from django import forms
from .models import Matkul

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = {'matkul'}

    error_messages = {
        'required' : 'Mohon diisi'
    }

    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama matkul'
    }

    matkul = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs))